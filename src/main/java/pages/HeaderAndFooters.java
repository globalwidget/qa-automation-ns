package pages;

import java.util.Set;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class HeaderAndFooters extends ApplicationKeywords {

	// Header
	private static final String home = "Home #xpath=//a[contains(text(),'Home')]";
	private static final String shop = "Shop #xpath=//li[contains(@id,'menu')]/a[contains(@href,'shop')]";
	private static final String wholesale = "Wholesale #xpath=//a[contains(text(),'Wholesale')]";
	private static final String learn = "Learn #xpath=//a[contains(text(),'Learn')]";
	private static final String contact = "Contact #xpath=//a[contains(text(),'Contact')]";
	private static final String privacy = "Privacy #xpath=//a[contains(text(),'Privacy')]";
	private static final String newsLetter = "Newsletter #xpath=//a[contains(text(),'Get 15% Off')]";
	private static final String phoneNumber = "Phone Number #xpath=//span[contains(text(),'1.800.713.6405')]";
	private static final String myAccount = "Account #xpath=//span[contains(text(),'My account')]";
	private static final String cart = "Cart #xpath=//span[contains(text(),'Cart')]";
	private static final String shop_NewCBDproducts = "New CBD Products #xpath=//a[contains(text(),'New CBD Products')]";
	private static final String shop_CBDEdibles = "CBD Edibles #xpath=//a[contains(text(),'CBD Edibles')]";
	private static final String shop_CBDEdibles_CBDGummies = "CBD Gummies #xpath=//a[contains(text(),'CBD Edibles')]/..//a[contains(text(),'CBD Gummies')]";
//	private static final String shop_CBDEdibles_CBDrelaxationShot = "CBD Relaxation Shot #xpath=//a[contains(text(),'CBD Relaxation Shot')]";
	private static final String shop_CBDEdibles_CBDoil = "CBD Edibles - CBD Oil #xpath=//li[contains(@id,'403')]/a[contains(text(),'CBD Oil')]";
	private static final String shop_CBDEdibles_CBDCapsules = "CBD Capsules #xpath=//a[contains(text(),'CBD Edibles')]/..//a[contains(text(),'CBD Capsules')]";
	private static final String shop_CBDEdibles_CBDSyrup = "CBD Syrup #xpath=//a[contains(text(),'CBD Syrup')]";
	private static final String shop_CBDtopicals = "CBD Topicals #xpath=//ul//a[contains(text(),'CBD Topicals') and not(contains(text(),'Self-Care'))]";
	private static final String shop_CBDtopicals_painReliefGel = "Pain Relief Rub #xpath=//a[contains(text(),'CBD Topicals')]/..//a[contains(text(),'CBD Pain Relief')]";
	private static final String shop_CBDtopicals_CBDlipBalm = "CBD Lip Balm #xpath=//li/a[contains(text(),'CBD Lip Balm')]";
	private static final String shop_CBDtopicals_CBDbathBombs = "CBD Bath Bombs #xpath=//a[contains(text(),'CBD Bath Bombs')]";
	private static final String shop_CBDtopicals_CBDessentialOilRollers = "CBD Essential Oil Rollers #xpath=//a[contains(text(),'CBD Essential Oil Rollers')]";
	private static final String shop_CBDtopicals_CBDHeatReliefSpray = "CBD Heat Relief Spray #xpath=//a[contains(text(),'CBD Heat Relief Spray')]";
	private static final String shop_CBDtopicals_CBDlotion = "CBD Lotion #xpath=//a[contains(text(),'CBD Lotion')]";
	private static final String shop_CBDtopicals_CBDpatches = "CBD Patches #xpath=//a[contains(text(),'CBD Patches')]";
	private static final String shop_CBDpetProdcuts = "Pet CBD Products #xpath=//a[contains(text(),'CBD Pet Products')]";
//	private static final String shop_CBDpetProdcuts_CBDpetoils = "Pet CBD Oils #xpath=//a[contains(text(),'CBD Pet Oil')]";
	private static final String shop_CBDpetProdcuts_CBDdogTreats = "Pet CBD Dog Treats #xpath=//a[contains(text(),'CBD Dog Treats')]";
	private static final String shop_CBDgummies = "CBD Gummies #xpath=//li[contains(@id,'406')]/a[contains(text(),'Gummies')]";
	private static final String shop_CBDoil = "CBD Oil #xpath=//li[contains(@id,'403')]/a[contains(text(),'CBD Oil') ]";
	private static final String shop_CBDcapsules = "CBD Capsules #xpath=//li[@id='menu-item-405']/a";
	private static final String shop_cbdPainReliefRub = "CBD Pain Relief Rub #xpath=//li[@id='menu-item-57784']";

	private static final String wholesale_TermsConditions = "Wholesale Terms and Conditions #xpath=//li/a[contains(text(),'Terms and Conditions')]";
	private static final String wholesale_BecomeDistributor = "Become Distributor #xpath=//a[contains(text(),'Become a Distributor')]";

	private static final String learn_ProductFinderQuiz = "Product Finder Quiz #xpath=//a[contains(text(),'Product Finder Quiz')]";
	private static final String learn_OurStory = "Our Story #xpath=//a[contains(text(),'Our Story')]";
	private static final String learn_Blog = "Blog #xpath=//li[@id='menu-item-1395']/a";
	private static final String learn_CBDoilBenefits = "Benefits of CBD Oil #xpath=//a[contains(text(),'CBD Oil Benefits')]";
	private static final String learn_InTheNews = "In the News #xpath=//a[contains(text(),'In the News')]";
	private static final String learn_HempRecipes = "Hemp Reciepies #xpath=//a[contains(text(),'Hemp Recipes')]";
	private static final String learn_LabTests = "Lab Tests #xpath=//a[contains(text(),'Lab Tests')]";
	private static final String learn_Faq = "Faq #xpath=//li[@id='menu-item-26']/a";

	private static final String facebookHeaderLink = "Facebook Header Link #xpath=//div[@class='top-bar-service-wrap']//a[contains(@href,'facebook')]";
	private static final String instagramHeaderLink = "Instagram Header Link #xpath=//div[@class='top-bar-service-wrap']//a[contains(@href,'instagram')]";
	private static final String twitterHeaderLink = "Twitter Header Link #xpath=//div[@class='top-bar-service-wrap']//a[contains(@href,'twitter')]";
	private static final String youtubeHeaderLink = "Youtube Header Link #xpath=//div[@class='top-bar-service-wrap']//a[contains(@href,'youtube')]";

	private static final String contactUsHeader = "Contact Us Header #xpath=//h1[contains(text(),'Contact Us')]";
	private static final String privacyPolicyHeader = "Privacy Policy Header #xpath=//h1[contains(text(),'Privacy Policy')]";
	private static final String newsletterHeader = "Newsletter Header #xpath=//h1[contains(text(),'Special Offer for Email Subscribers')]";
	private static final String MyCartHeader = "My Cart Header #xpath=//h1[contains(text(),'Cart')]";
	private static final String emptyCartMessage = "Empty Cart Message #xpath=//p[contains(text(),'Your cart is currently empty.')]";
	private static final String slide_HomePage = "Home Banner Slide #xpath=(//div[contains(@class,'banner')]//img)[1]";
	private static final String shopHeader = "Shop Header #xpath=//h1[contains(text(),'Shop Premium CBD Products')]";
	private static final String newCBDProductsHeader = "New CBD Products Header #xpath=//h1[contains(text(),'Shop New CBD Products')]";
	private static final String cbdEdiblesHeader = "CBD Edibles Header #xpath=//h1[contains(text(),'CBD Edibles')]";
	private static final String cbdGummiesHeader = "CBD Gummies Header #xpath=//div/h1[contains(text(),'CBD GUMMIES')]";
	private static final String cbdCapsulesHeader = "CBD Capsules Header #xpath=//h1[contains(text(),'CBD CAPSULES')]";
//	private static final String cbdRelaxationShotHeader = "CBD Relaxation Shot Header #xpath=//h1[contains(text(),'CBD Relaxation Shot')]";
	private static final String cbdSyrupHeader = "CBD Syrup Header #xpath=//h1[contains(text(),'CBD SYRUP')]";
	private static final String cbdtopicalsHeader = "CBD Topicals Header #xpath=//h1[contains(text(),'PREMIUM CBD TOPICALS')]";
	private static final String cbdPainReliefGel = "CBD Pain Relief Rub Header #xpath=//h1[contains(text(),'CBD Pain Relief Gel')]";
	private static final String cbdLipBalmHeader = "CBD Lip Balm Header #xpath=//h1[contains(text(),'CBD Lip Balm')]";
	private static final String cbdBathBombsHeader = "CBD Bath Bombs Header #xpath=//h1[contains(text(),'PREMIUM CBD BATH BOMBS')]";
	private static final String cbdEssentialOilRollersHeader = "CBD Essential Oil Rollers Header #xpath=//h1[contains(text(),'CBD Essential Oil Rollers')]";
	private static final String cbdHeatReliefSprayHeader = "CBD Heat Relief Spray Header #xpath=//h1[contains(text(),'PREMIUM CBD HEAT RELIEF SPRAY')]";
	private static final String cbdLotionHeader = "CBD Lotion Header #xpath=//h1[contains(text(),'PREMIUM CBD LOTION')]";
	private static final String cbdPatchesHeader = "CBD Patches Header #xpath=//h1[contains(text(),'PREMIUM CBD PATCHES')]";

	private static final String petCBDproductsHeader = "Pet CBD Products Header #xpath=//h1[contains(text(),'PREMIUM CBD PET PRODUCTS')]";
	private static final String petCBDdogtreatsHeader = "Pet CBD Dog Treats Header #xpath=//h1[contains(text(),'CBD Dog Treats')]";
	private static final String petCBDoilHeader = "Pet CBD Oil Header #xpath=//h1[contains(text(),'PREMIUM CBD PET OIL')]";
	private static final String cbdOilHeader = "CBD Oil Header #xpath=//h1[contains(text(),'PREMIUM CBD OIL')]";

	private static final String learnHeader = "Learn Header #xpath=//h1[contains(text(),'Learn About CBD')]";
	private static final String productFinderQuizHeader = "Product Finder Quiz Header #xpath=//h1[contains(text(),'CBD Product Finder Quiz')]";
	private static final String ourStoryHeader = "Our Story Header #xpath=//h1[contains(text(),'Our Story')]";
	private static final String cbdBlogHeader = "Nature Script's CBD Blog Header #xpath=//h1[contains(text(),'Script CBD Blog')]";
	private static final String cbdoilBenefitsHeader = "CBD Oil Benefits Header #xpath=//h1[contains(text(),'Hemp CBD Oil Benefits')]";
	private static final String intheNewsHeader = "In the News Header #xpath=//h1[contains(text(),'IN THE NEWS')]";
	private static final String hempRecipesHeader = "Hemp Recipes Header #xpath=//h1[contains(text(),'CBD Hemp Recipes')]";
	private static final String labtestingResultsHeader = "Lab Testing Results Header #xpath=//h1[contains(text(),'CBD Lab Testing Results')]";
	private static final String faqHeader = "FAQ Header #xpath=//h1[contains(text(),'FREQUENTLY ASKED QUESTIONS')]";
	private static final String wholesaleHeader = "Wholesale Header #xpath=//h1[contains(text(),'Wholesale CBD')]";
	private static final String wholesaleTermsConditionsHeader = "Wholesale Terms and Conditions Header #xpath=//h1[contains(text(),'Wholesale Terms and Conditions')]";
//	private static final String wholesalebecomeDistributorHeader = "Become a Distributor Header #xpath=//h1[contains(text(),'Become a CBD Distributor')]";

	// Footer
	private static final String homeFooterLink = "Home Footer Link #xpath=//a[contains(text(),'HOME')]";
	private static final String shopFooterLink = "Shop Footer Link #xpath=//a[contains(text(),'SHOP')]";
	private static final String wholesaleFooterLink = "Wholesale Footer Link #xpath=//a[contains(text(),'WHOLESALE')]";
	private static final String newsletterFooterLink = "Newsletter Footer Link #xpath=//a[contains(text(),'GET 15% OFF')]";
	private static final String shippingPolicyFooterLink = "Shipping Policy Footer Link #xpath=//a[contains(text(),'SHIPPING POLICY')]";
	private static final String termsFooterLink = "Terms Footer Link #xpath=//a[contains(text(),'TERMS')]";
	private static final String faqFooterLink = "Faq Footer Link #xpath=//div[contains(@id,'footer')]/a[contains(text(),'FAQ')]";
	private static final String refundFooterLink = "Refund Footer Link #xpath=//a[contains(text(),'REFUND')]";
	private static final String contactFooterLink = "Contact Footer Link #xpath=//a[contains(text(),'CONTACT')]";
	private static final String ccpaFooterLink = "CCPA Footer Link #xpath=//a[contains(text(),'CCPA')]";
	private static final String facebookFooterLink = "Facebook Footer Link #xpath=//div[contains(@id,'footer')]/a[contains(@href,'facebook')]";
	private static final String instagramFooterLink = "Instagram Footer Link #xpath=//div[contains(@id,'footer')]/a[contains(@href,'instagram')]";
	private static final String youtubeFooterLink = "Youtube Footer Link #xpath=//div[contains(@id,'footer')]/a[contains(@href,'youtube')]";
	private static final String twitterFooterLink = "Twitter Footer Link #xpath=//div[contains(@id,'footer')]/a[contains(@href,'twitter')]";
	private static final String pinterestFooterLink = "Pinterest Footer Link #xpath=//div[contains(@id,'footer')]/a[contains(@href,'pinterest')]";
	private static final String premiumHempbombsFooterLink = "Premium Hemp Bombs Footer Link #xpath=	//a[contains(text(),'Premium Hemp Products Natures Script')]";

	private static final String shippingPolicyHeader = "Shipping Policy Header #xpath=//h1[contains(text(),'Shipping Policy')]";
	private static final String termsConditionsHeader = "Terms and Conditions Header #xpath=//h1[contains(text(),'Terms & Conditions')]";
	private static final String refundPolicyHeader = "Relief Policy Header #xpath=//h1[contains(text(),'Refund Policy')]";
	private static final String ccpaHeader = "CCPA Header Header #xpath=//h1[contains(text(),'CCPA Data Request')]";

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to go to my account page after User is logged in.
	 */
	public void goToMyAccount() {
		try {
			if (isElementPresent(myAccount)) {
				highLighterMethod(myAccount);
				clickOn(myAccount);
			} else {
				testStepFailed("Could not go to My Account page", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on My Account successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Phone Number
	 */
	public void Verifypresence_PhoneNumber() {
		try {
			if (isElementPresent(phoneNumber) && findWebElement(phoneNumber).isEnabled()) {
				highLighterMethod(phoneNumber);
				testStepInfo("The Number present in Call Us is : " + getText(phoneNumber));
			} else {
				testStepFailed("Phone Number is not present or is not enabled in Header");
			}
		} catch (Exception e) {
			testStepFailed("Presence of Phone Number in Header could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Facebook link in Header
	 */
	public void clickFacebookIcon_Header() {
		try {
			if (isElementPresent(facebookHeaderLink)) {
				highLighterMethod(facebookHeaderLink);
				clickOn(facebookHeaderLink);
			} else {
				testStepFailed("Facebook Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Facebook Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Instagram link in Header
	 */
	public void clickInstagramIcon_Header() {
		try {
			if (isElementPresent(instagramHeaderLink)) {
				highLighterMethod(instagramHeaderLink);
				clickOn(instagramHeaderLink);
			} else {
				testStepFailed("Instagram Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Instagram Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Twitter link in Header
	 */
	public void clickTwitterIcon_Header() {
		try {
			if (isElementPresent(twitterHeaderLink)) {
				highLighterMethod(twitterHeaderLink);
				clickOn(twitterHeaderLink);
			} else {
				testStepFailed("Twitter Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Twitter Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Youtube link in Header
	 */
	public void clickYoutubeIcon_Header() {
		try {
			if (isElementPresent(youtubeHeaderLink)) {
				highLighterMethod(youtubeHeaderLink);
				clickOn(youtubeHeaderLink);
			} else {
				testStepFailed("Youtube Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Youtube Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on My cart in Header
	 */
	public void clickOnMyCart() {

		try {
			if (isElementPresent(cart)) {
				highLighterMethod(cart);
				clickOn(cart);
			} else {
				testStepFailed("My Cart not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Cart could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on home in Header.
	 */
	public void goTo_Home() {

		try {
			if (isElementPresent(home)) {
				highLighterMethod(home);
				clickOn(home);
			} else {
				testStepFailed("Home not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Home could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on desired option from Shop menu
	 */
	public void goTo_Shop(int subMenu, int subMenu_2) {

		try {
			if (subMenu == 0) {
				if (isElementPresent(shop)) {
					highLighterMethod(shop);
					clickOn(shop);
				} else {
					testStepFailed("Shop not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(shop_NewCBDproducts)) {
						navigateMenu_Two(shop, shop_NewCBDproducts);
					} else {
						testStepFailed("New CBD Products not present under Shop");
					}

					break;
				}

				case 2: {
					if (subMenu_2 == 0) {
						if (isElementPresent(shop_CBDEdibles)) {
							navigateMenu_Two(shop, shop_CBDEdibles);
						} else {
							testStepFailed("CBD Edibles not present under Shop Menu");
						}
						break;
					} else {
						switch (subMenu_2) {
						case 1: {
							if (isElementPresent(shop_CBDEdibles_CBDGummies)) {
								navigateMenu_Three(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies);
							} else {
								testStepFailed("CBD Gummies not present under CBD Edibles");
							}
							break;
						}

						case 2: {
							if (isElementPresent(shop_CBDEdibles_CBDCapsules)) {
								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
										shop_CBDEdibles_CBDCapsules);
							} else {
								testStepFailed("CBD Capsules not present under CBD Edibles");
							}
							break;
						}
						case 3: {
							if (isElementPresent(shop_CBDEdibles_CBDoil)) {
								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
										shop_CBDEdibles_CBDoil);
							} else {
								testStepFailed("CBD Oil not present under CBD Edibles");
							}
							break;
						}

						case 4: {
							if (isElementPresent(shop_CBDEdibles_CBDSyrup)) {
								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
										shop_CBDEdibles_CBDSyrup);
							} else {
								testStepFailed("CBD Syrup not present under CBD Edibles");
							}
							break;
						}

						}
					}
					break;
				}

				case 3: {
					if (subMenu_2 == 0) {
						if (isElementPresent(shop_CBDtopicals)) {
							navigateMenu_Two(shop, shop_CBDtopicals);
						} else {
							testStepFailed("CBD Topicals not present under Shop Menu");
						}
						break;
					} else {
						switch (subMenu_2) {
						case 1: {
							if (isElementPresent(shop_CBDtopicals_painReliefGel)) {
								navigateMenu_Three(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel);
							} else {
								testStepFailed("Pain Relief Gel not present under CBD Topicals");
							}
							break;
						}

						case 2: {
							if (isElementPresent(shop_CBDtopicals_CBDlipBalm)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel,
										shop_CBDtopicals_CBDlipBalm);
							} else {
								testStepFailed("Lip Balm not present under CBD Topicals");
							}
							break;
						}

						case 3: {
							if (isElementPresent(shop_CBDtopicals_CBDbathBombs)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel,
										shop_CBDtopicals_CBDbathBombs);
							} else {
								testStepFailed("CBD Bath Bombs not present under CBD Topicals");
							}
							break;
						}

						case 4: {
							if (isElementPresent(shop_CBDtopicals_CBDessentialOilRollers)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel,
										shop_CBDtopicals_CBDessentialOilRollers);
							} else {
								testStepFailed("Essential Oil Rollers not present under CBD Topicals");
							}
							break;
						}

						case 5: {
							if (isElementPresent(shop_CBDtopicals_CBDHeatReliefSpray)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel,
										shop_CBDtopicals_CBDHeatReliefSpray);
							} else {
								testStepFailed("Heat Relief Spray not present under CBD Topicals");
							}
							break;
						}

						case 6: {
							if (isElementPresent(shop_CBDtopicals_CBDlotion)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel,
										shop_CBDtopicals_CBDlotion);
							} else {
								testStepFailed("CBD Lotion not present under CBD Topicals");
							}
							break;
						}

						case 7: {
							if (isElementPresent(shop_CBDtopicals_CBDpatches)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painReliefGel,
										shop_CBDtopicals_CBDpatches);
							} else {
								testStepFailed("CBD Patches not present under CBD Topicals");
							}
							break;
						}
						}
					}
					break;
				}

				case 4: {
					if (subMenu_2 == 0) {
						if (isElementPresent(shop_CBDpetProdcuts)) {
							navigateMenu_Two(shop, shop_CBDpetProdcuts);
						} else {
							testStepFailed("CBD Pet Products not present under Shop Menu");
						}
						break;
					} else {
						switch (subMenu_2) {

						case 1: {
							if (isElementPresent(shop_CBDpetProdcuts_CBDdogTreats)) {
								navigateMenu_Three(shop, shop_CBDpetProdcuts, shop_CBDpetProdcuts_CBDdogTreats);
							} else {
								testStepFailed("CBD Dog Treats not present under CBD Pet Products");
							}
							break;
						}

//						case 2: {
//							if (isElementPresent(shop_CBDpetProdcuts_CBDpetoils)) {
//								navigateMenu_Three(shop, shop_CBDpetProdcuts, shop_CBDpetProdcuts_CBDpetoils);
//							} else {
//								testStepFailed("CBD Pet Oils not present under CBD Pet Products");
//							}
//							break;
//						}
						}
					}
					break;
				}

				case 5: {
					if (isElementPresent(shop_CBDgummies)) {
						navigateMenu_Two(shop, shop_CBDgummies);
					} else {
						testStepFailed("CBD Gummies not present under Shop Menu");
					}
					break;
				}

				case 6: {
					if (isElementPresent(shop_CBDoil)) {
						navigateMenu_Two(shop, shop_CBDoil);
					} else {
						testStepFailed("CBD Oil not present under Shop Menu");
					}
					break;
				}

				case 7: {
					if (isElementPresent(shop_CBDcapsules)) {
						navigateMenu_Two(shop, shop_CBDcapsules);
					} else {
						testStepFailed("CBD Capsules not present under Shop Menu");
					}
					break;
				}

				case 8: {
					if (isElementPresent(shop_cbdPainReliefRub)) {
						navigateMenu_Two(shop, shop_cbdPainReliefRub);
					} else {
						testStepFailed("CBD Pain Relief Rub not present under Shop Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the Shop menu could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on desired option from Learn menu
	 */
	public void goTo_Learn(int subMenu) {
		try {
			if (subMenu == 0) {
				if (isElementPresent(learn)) {
					highLighterMethod(learn);
					clickOn(learn);
				} else {
					testStepFailed("Learn not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(learn_ProductFinderQuiz)) {
						navigateMenu_Two(learn, learn_ProductFinderQuiz);
					} else {
						testStepFailed("Product Finder Quiz not present under Learn Menu");
					}
					break;
				}

				case 2: {
					if (isElementPresent(learn_OurStory)) {
						navigateMenu_Two(learn, learn_OurStory);
					} else {
						testStepFailed("Our Story not present under Learn Menu");
					}
					break;
				}

				case 3: {
					if (isElementPresent(learn_Blog)) {
						navigateMenu_Two(learn, learn_Blog);
					} else {
						testStepFailed("Blog not present under Learn Menu");
					}
					break;
				}
				case 4: {
					if (isElementPresent(learn_CBDoilBenefits)) {
						navigateMenu_Two(learn, learn_CBDoilBenefits);
					} else {
						testStepFailed("CBD Oil Benefits not present under Learn Menu");
					}
					break;
				}

				case 5: {
					if (isElementPresent(learn_InTheNews)) {
						navigateMenu_Two(learn, learn_InTheNews);
					} else {
						testStepFailed("In the News not present under Learn Menu");
					}
					break;
				}

				case 6: {
					if (isElementPresent(learn_HempRecipes)) {
						navigateMenu_Two(learn, learn_HempRecipes);
					} else {
						testStepFailed("Hemp Recipes not present under Learn Menu");
					}
					break;
				}

				case 7: {
					if (isElementPresent(learn_LabTests)) {
						navigateMenu_Two(learn, learn_LabTests);
					} else {
						testStepFailed("Lab Tests not present under Learn Menu");
					}
					break;
				}
				case 8: {
					if (isElementPresent(learn_Faq)) {
						navigateMenu_Two(learn, learn_Faq);
					} else {
						testStepFailed("FAQ not present under Learn Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the Learn menu could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on desired option from Wholesale menu
	 */
	public void goTo_Wholesale(int subMenu) {
		try {
			if (subMenu == 0) {
				if (isElementPresent(wholesale)) {
					highLighterMethod(wholesale);
					clickOn(wholesale);
				} else {
					testStepFailed("Wholesale not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(wholesale_TermsConditions)) {
						navigateMenu_Two(wholesale, wholesale_TermsConditions);
					} else {
						testStepFailed("Terms And Conditions not present under Wholesale Menu");
					}
					break;
				}
				case 2: {
					if (isElementPresent(wholesale_BecomeDistributor)) {
						navigateMenu_Two(wholesale, wholesale_BecomeDistributor);
					} else {
						testStepFailed("Become Distributor not present under Wholesale Menu");
					}
					break;
				}

				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the Wholesale menu could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on contact
	 */
	public void goTo_Contact() {

		try {
			if (isElementPresent(contact)) {
				highLighterMethod(contact);
				clickOn(contact);
			} else {
				testStepFailed("Contact not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Contact could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to verify the function of privacy policy
	 */
	public void Verify_PrivacyPolicy() {
		try {
			if (isElementPresent(privacy)) {
				highLighterMethod(privacy);
				clickOn(privacy);
				if (isElementDisplayed(privacyPolicyHeader)) {
					highLighterMethod(privacyPolicyHeader);
					testStepInfo("The privacy policy page opened successfully");
				} else
					testStepFailed("The privacy policy page could not be opened");
			} else {
				testStepFailed("Privacy not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Privacy could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to go to Newsletter subscription page
	 */
	public void goTo_Newsletter() {

		try {
			if (isElementPresent(newsLetter)) {
				highLighterMethod(newsLetter);
				clickOn(newsLetter);
			} else {
				testStepFailed("'Get 15% off' not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Could not navigate to Newsletter Subscription page");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to verify the routing.
	 */
	public void verifyNavigation(String option) {

		try {
			switch (option) {
			case "cart": {
				if (isElementDisplayed(MyCartHeader)) {
					highLighterMethod(MyCartHeader);
					testStepInfo("The relevant page was opened upon clicking My Cart in Header");
				} else {
					testStepFailed("My Cart Title was not present");
				}
				break;
			}

			case "emptyCart": {
				if (isElementDisplayed(emptyCartMessage)) {
					highLighterMethod(emptyCartMessage);
					testStepInfo("Empty Cart message was displayed");
				} else {
					testStepFailed("Empty Cart message was not displayed");
				}
				break;
			}

			case "newsletter": {
				if (isElementDisplayed(newsletterHeader)) {
					testStepInfo("The relevant page was opened upon clicking 'Get 15% off' in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking 'Get 15% off' in Header");
				}
				break;
			}

			case "homeBanner": {
				waitForElementToDisplay(slide_HomePage, 30);
				if (isElementDisplayed(slide_HomePage)) {
					scrollToViewElement(slide_HomePage);
					highLighterMethod(slide_HomePage);
					testStepInfo("The relevant page was opened upon clicking Home in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Home in Header");
				}
				break;
			}

			case "shop": {
				if (isElementDisplayed(shopHeader)) {
					highLighterMethod(shopHeader);
					testStepInfo("The relevant page was opened upon clicking Shop in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shop in Header");
				}
				break;
			}

			case "newCBDProducts": {
				if (isElementDisplayed(newCBDProductsHeader)) {
					highLighterMethod(newCBDProductsHeader);
					testStepInfo("The relevant page was opened upon clicking New CBD Products under Shop");
				} else {
					testStepFailed("The relevant page was not opened upon clicking New CBD Products under Shop");
				}
				break;
			}

			case "cbdEdibles": {
				if (isElementDisplayed(cbdEdiblesHeader)) {
					highLighterMethod(cbdEdiblesHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Edibles under Shop");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Edibles under Shop");
				}
				break;
			}
			case "Gummies": {
				if (isElementDisplayed(cbdGummiesHeader)) {
					highLighterMethod(cbdGummiesHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Gummies under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Gummies under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "capsules": {
				if (isElementDisplayed(cbdCapsulesHeader)) {
					highLighterMethod(cbdCapsulesHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Capsules under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Capsules under CBD Edibles in Shop Menu");
				}
				break;
			}

//			case "relaxationShot": {
//				if (isElementDisplayed(cbdRelaxationShotHeader)) {
//					highLighterMethod(cbdRelaxationShotHeader);
//					testStepInfo(
//							"The relevant page was opened upon clicking CBD Relaxation Shot under CBD Edibles in Shop Menu");
//				} else {
//					testStepFailed(
//							"The relevant page was not opened upon clicking CBD Relaxation Shot under CBD Edibles in Shop Menu");
//				}
//				break;
//			}

			case "cbdSyrup": {
				if (isElementDisplayed(cbdSyrupHeader)) {
					highLighterMethod(cbdSyrupHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Syrup under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Syrup under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "cbdTopicals": {
				if (isElementDisplayed(cbdtopicalsHeader)) {
					highLighterMethod(cbdtopicalsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Topicals in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdPainReliefGel": {
				if (isElementDisplayed(cbdPainReliefGel)) {
					highLighterMethod(cbdPainReliefGel);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Pain Relief Gel under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Pain Relief Gel under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdLipBalm": {
				if (isElementDisplayed(cbdLipBalmHeader)) {
					highLighterMethod(cbdLipBalmHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Lip Balm under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Lip Balm under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdBathBombs": {
				if (isElementDisplayed(cbdBathBombsHeader)) {
					highLighterMethod(cbdBathBombsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Bath Bombs under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Bath Bombs under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdEssentialOilRollers": {
				if (isElementDisplayed(cbdEssentialOilRollersHeader)) {
					highLighterMethod(cbdEssentialOilRollersHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Essential Oil Rollers under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Essential Oil Rollers under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdHeatReliefSpray": {
				if (isElementDisplayed(cbdHeatReliefSprayHeader)) {
					highLighterMethod(cbdHeatReliefSprayHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Heat Relief Spray under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Heat Relief Spray under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdLotion": {
				if (isElementDisplayed(cbdLotionHeader)) {
					highLighterMethod(cbdLotionHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Lotion under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Lotion under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdPatches": {
				if (isElementDisplayed(cbdPatchesHeader)) {
					highLighterMethod(cbdPatchesHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Patches under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Patches under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "petCBDproducts": {
				if (isElementDisplayed(petCBDproductsHeader)) {
					highLighterMethod(petCBDproductsHeader);
					testStepInfo("The relevant page was opened upon clicking Pet CBD Products in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "petCBDoil": {
				if (isElementDisplayed(petCBDoilHeader)) {
					highLighterMethod(petCBDoilHeader);
					testStepInfo(
							"The relevant page was opened upon clicking Pet CBD Oil under Pet CBD Products in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Pet CBD Oil under Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "petCBDdogtreats": {
				if (isElementDisplayed(petCBDdogtreatsHeader)) {
					highLighterMethod(petCBDdogtreatsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking Pet CBD Dog Treats under Pet CBD Products in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Pet CBD Dog Treats under Pet CBD Products in Shop Menu");
				}
				break;
			}
			case "Shop_Gummies": {
				if (isElementDisplayed(cbdGummiesHeader)) {
					highLighterMethod(cbdGummiesHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Gummies in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Gummies in Shop Menu");
				}
				break;
			}

			case "Shop_capsules": {
				if (isElementDisplayed(cbdCapsulesHeader)) {
					highLighterMethod(cbdCapsulesHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Capsules in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Capsules in Shop Menu");
				}
				break;
			}

			case "Shop_cbdPainReliefRub": {
				if (isElementDisplayed(cbdPainReliefGel)) {
					highLighterMethod(cbdPainReliefGel);
					testStepInfo("The relevant page was opened upon clicking CBD Pain Relief Rub in Shop Menu");
				} else {
					testStepFailed("The relevant page was opened upon clicking CBD Pain Relief Rub in Shop Menu");
				}
				break;
			}

			case "cbdOil": {
				if (isElementDisplayed(cbdOilHeader)) {
					highLighterMethod(cbdOilHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Oil in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Oil in Shop Menu");
				}
				break;
			}

			case "wholesale": {
				if (isElementDisplayed(wholesaleHeader)) {
					highLighterMethod(wholesaleHeader);
					testStepInfo("The relevant page was opened upon clicking Wholesale");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Wholesale");
				}
				break;
			}

//			case "wholesaleBecomeDistributor": {
//				if (isElementDisplayed(wholesalebecomeDistributorHeader)) {
//					highLighterMethod(wholesalebecomeDistributorHeader);
//					testStepInfo("The relevant page was opened upon clicking Become Distributor in Wholesale Menu");
//				} else {
//					testStepFailed(
//							"The relevant page was not opened upon clicking Become Distributor in Wholesale Menu");
//				}
//				break;
//			}

			case "wholesaleTermsConditions": {
				if (isElementDisplayed(wholesaleTermsConditionsHeader)) {
					highLighterMethod(wholesaleTermsConditionsHeader);
					testStepInfo("The relevant page was opened upon clicking Terms and Conditions in Wholesale Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Terms and Conditions in Wholesale Menu");
				}
				break;
			}

			case "learn": {
				if (isElementDisplayed(learnHeader)) {
					highLighterMethod(learnHeader);
					testStepInfo("The relevant page was opened upon clicking Learn");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Learn");
				}
				break;
			}

			case "cbdblog": {
				if (isElementDisplayed(cbdBlogHeader)) {
					highLighterMethod(cbdBlogHeader);
					testStepInfo("The relevant page was opened upon clicking Blog in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Blog in Learn Menu");
				}
				break;
			}

			case "ourStory": {
				if (isElementDisplayed(ourStoryHeader)) {
					highLighterMethod(ourStoryHeader);
					testStepInfo("The relevant page was opened upon clicking Our Story in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Our Story in Learn Menu");
				}
				break;
			}

			case "CBDoilbenefits": {
				if (isElementDisplayed(cbdoilBenefitsHeader)) {
					highLighterMethod(cbdoilBenefitsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Oil Benefits in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Oil Benefits in Learn Menu");
				}
				break;
			}

			case "intheNews": {
				if (isElementDisplayed(intheNewsHeader)) {
					highLighterMethod(intheNewsHeader);
					testStepInfo("The relevant page was opened upon clicking In The News in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking In The News in Learn Menu");
				}
				break;
			}

			case "hempRecipe": {
				if (isElementDisplayed(hempRecipesHeader)) {
					highLighterMethod(hempRecipesHeader);
					testStepInfo("The relevant page was opened upon clicking Hemp Recipe in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Hemp Recipe in Learn Menu");
				}
				break;
			}

			case "labTesting": {
				if (isElementDisplayed(labtestingResultsHeader)) {
					highLighterMethod(labtestingResultsHeader);
					testStepInfo("The relevant page was opened upon clicking Third Party Lab Testing in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Third Lab Testing in Learn Menu");
				}
				break;
			}

			case "faq": {
				if (isElementDisplayed(faqHeader)) {
					highLighterMethod(faqHeader);
					testStepInfo("The relevant page was opened upon clicking FAQ in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking FAQ in Learn Menu");
				}
				break;
			}

			case "productFinderQuiz": {
				if (isElementDisplayed(productFinderQuizHeader)) {
					highLighterMethod(productFinderQuizHeader);
					testStepInfo("The relevant page was opened upon clicking Product Finder Quiz in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Product Finder Quiz in Learn Menu");
				}
				break;
			}

			case "contact": {
				if (isElementDisplayed(contactUsHeader)) {
					highLighterMethod(contactUsHeader);
					testStepInfo("The relevant page was opened upon clicking contact");
				} else {
					testStepFailed("The relevant page was not opened upon clicking contact");
				}
				break;
			}

			case "terms": {
				if (isElementDisplayed(termsConditionsHeader)) {
					highLighterMethod(termsConditionsHeader);
					testStepInfo("The relevant page was opened upon clicking Terms in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Terms in Footer");
				}
				break;
			}

			case "refundPolicy": {
				if (isElementDisplayed(refundPolicyHeader)) {
					highLighterMethod(refundPolicyHeader);
					testStepInfo("The relevant page was opened upon clicking Refund Policy in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Refund Policy in Footer");
				}
				break;
			}

			case "shippingPolicy": {
				if (isElementDisplayed(shippingPolicyHeader)) {
					highLighterMethod(shippingPolicyHeader);
					testStepInfo("The relevant page was opened upon clicking Shipping Policy in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shipping Policy in Footer");
				}
				break;
			}

			case "facebook": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.facebook.com/NaturesScriptHemp")
						|| driver.getCurrentUrl().contains(
								"https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FNaturesScriptHemp")) {
					testStepInfo("Facebook page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Facebook page of HempBombs could not be opened");
				}
				break;
			}

			case "instagram": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.instagram.com/natures_script")
						|| driver.getCurrentUrl().contains("https://www.instagram.com/accounts/login")) {
					testStepInfo("Instagram page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Instagram page of HempBombs could not be opened");
				}
				break;
			}

			case "youtube": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.youtube.com/channel/UCEsTAEpmXc035S6MnYoEy7Q")) {
					testStepInfo("Youtube page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Youtube page of HempBombs could not be opened");
				}
				break;
			}

			case "twitter": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://twitter.com/Natures_Script")) {
					testStepInfo("Twitter page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Twitter page of HempBombs could not be opened");
				}
				break;
			}

			case "pinterest": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.pinterest.com/natures_script")) {
					testStepInfo("Pinterest page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Pinterest page of HempBombs could not be opened");
				}
				break;
			}

			case "ccpa": {
				switchToLastTab();
				if (isElementDisplayed(ccpaHeader)) {
					highLighterMethod(ccpaHeader);
					testStepInfo("The relevant page was opened upon clicking CCPA in Footer");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("The relevant page was not opened upon clicking CCPA in Footer");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Relevant verification could not be done successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to navigate through Footer.
	 */
	public void navigateFooter(String option) {

		try {
			switch (option) {
			case "home": {
				if (isElementDisplayed(homeFooterLink)) {
					highLighterMethod(homeFooterLink);
					clickOn(homeFooterLink);
				} else {
					testStepFailed("Home Footer not displayed in Footer");
				}
				break;
			}

			case "shop": {
				if (isElementPresent(shopFooterLink)) {
					highLighterMethod(shopFooterLink);
					clickOn(shopFooterLink);
				} else {
					testStepFailed("Shop not displayed in Footer");
				}
				break;
			}

			case "wholesale": {
				if (isElementPresent(wholesaleFooterLink)) {
					highLighterMethod(wholesaleFooterLink);
					clickOn(wholesaleFooterLink);
				} else {
					testStepFailed("Wholesale was not displayed in Footer");
				}
				break;
			}

			case "newsletter": {
				if (isElementPresent(newsletterFooterLink)) {
					highLighterMethod(newsletterFooterLink);
					clickOn(newsletterFooterLink);
				} else {
					testStepFailed("Newsletter not displayed in Footer");
				}
				break;
			}

			case "shippingPolicy": {
				if (isElementPresent(shippingPolicyFooterLink)) {
					highLighterMethod(shippingPolicyFooterLink);
					clickOn(shippingPolicyFooterLink);
				} else {
					testStepFailed("Shipping Policy not displayed in Footer");
				}
				break;
			}

			case "terms": {
				if (isElementPresent(termsFooterLink)) {
					highLighterMethod(termsFooterLink);
					clickOn(termsFooterLink);
				} else {
					testStepFailed("Terms not displayed in Footer");
				}
				break;
			}

			case "faq": {
				if (isElementPresent(faqFooterLink)) {
					highLighterMethod(faqFooterLink);
					clickOn(faqFooterLink);
				} else {
					testStepFailed("FAQ not displayed in Footer");
				}
				break;
			}

			case "refund": {
				if (isElementPresent(refundFooterLink)) {
					highLighterMethod(refundFooterLink);
					clickOn(refundFooterLink);
				} else {
					testStepFailed("Refund not displayed in Footer");
				}
				break;
			}

			case "contact": {
				if (isElementPresent(contactFooterLink)) {
					highLighterMethod(contactFooterLink);
					clickOn(contactFooterLink);
				} else {
					testStepFailed("Contact not displayed in Footer");
				}
				break;
			}

			case "facebook": {
				if (isElementPresent(facebookFooterLink)) {
					highLighterMethod(facebookFooterLink);
					clickOn(facebookFooterLink);
				} else {
					testStepFailed("Facebook Icon not displayed in Footer");
				}
				break;
			}

			case "instagram": {
				if (isElementPresent(instagramFooterLink)) {
					highLighterMethod(instagramFooterLink);
					clickOn(instagramFooterLink);
				} else {
					testStepFailed("Instagram Icon not displayed in Footer");
				}
				break;
			}

			case "twitter": {
				if (isElementPresent(twitterFooterLink)) {
					highLighterMethod(twitterFooterLink);
					clickOn(twitterFooterLink);
				} else {
					testStepFailed("Twitter Icon not displayed in Footer");
				}
				break;
			}

			case "youtube": {
				if (isElementPresent(youtubeFooterLink)) {
					highLighterMethod(youtubeFooterLink);
					clickOn(youtubeFooterLink);
				} else {
					testStepFailed("Youtube Icon not displayed in Footer");
				}
				break;
			}

			case "pinterest": {
				if (isElementPresent(pinterestFooterLink)) {
					highLighterMethod(pinterestFooterLink);
					clickOn(pinterestFooterLink);
				} else {
					testStepFailed("Pinterest Icon not displayed in Footer");
				}
				break;
			}

			case "ccpa": {
				if (isElementPresent(ccpaFooterLink)) {
					highLighterMethod(ccpaFooterLink);
					clickOn(ccpaFooterLink);
				} else {
					testStepFailed("CCPA not displayed in Footer");
				}
				break;
			}

			case "premiumHempbombs": {
				if (isElementPresent(premiumHempbombsFooterLink)) {
					highLighterMethod(premiumHempbombsFooterLink);
					clickOn(premiumHempbombsFooterLink);
				} else {
					testStepFailed("premiumHempbombs Link not displayed in Footer");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested navigation in footer could not be done");
			e.printStackTrace();
		}
	}
}
