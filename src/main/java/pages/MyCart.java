package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class MyCart extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//td[@data-title='Product']/a";
	private static final String removeProduct = "Remove Product #xpath=//td[@class='product-remove']/a";
	private static final String alertMessage = "Alert #xpath=//div[contains(@class,'message') and @role='alert']";
	private static final String alertMessage_invalidCoupon = "Alert #xpath=//ul[@role='alert']";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//input[@type='number']";
	private static final String updateCart = "Update Cart button #xpath=//button[contains(text(),'Update cart')]";
	private static final String couponCode = "Coupon Code #id=coupon_code";
	private static final String applyCoupon = "Apply Coupon #name=apply_coupon";
	private static final String proceedCheckout = "Proceed Checkout #xpath=//a[contains(text(),'Proceed to checkout')]";
	private static final String removeCoupon = "Remove Coupon #xpath=//td[contains(@data-title,'Coupon')]//a[contains(@href,'remove')]";

	public MyCart(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the cart
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			if (isElementPresent(productsPresent)) {
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					String productName_CartPage = product.getText();
					String productNamePartOne = "";
					String productNamePartTwo = "";
					if (productName.contains("�")) {
						productNamePartOne = productName.split("�")[0];
						productNamePartTwo = productName.split("�")[1];
						if (productName_CartPage.contains(productNamePartOne)
								&& productName_CartPage.contains(productNamePartTwo)) {
							productPresent = true;
							scrollToViewElement(product);
							highLighterMethod(product);
							manualScreenshot("The Product added is present in the cart");
							break;
						}
					}

					if (productName_CartPage.contains(productName)) {
						productPresent = true;
						scrollToViewElement(product);
						highLighterMethod(product);
						manualScreenshot("The Product added is present in the cart");
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in My Cart page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in My Cart page");
			} else
				testStepFailed("No products present in My cart page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on delete button in My Cart page
	 */
	public String clickOnRemoveProduct() {
		String productName = "";
		try {
			if (isElementDisplayed(removeProduct)) {
				highLighterMethod(removeProduct);
				productName = findWebElements(productsPresent).get(0).getText();
				findWebElements(removeProduct).get(0).click();
				GOR.productAdded = false;
				testStepInfo("The remove button was clicked for the product " + productName);
				return productName;
			} else
				testStepFailed("Could not find any product in My Cart");
		} catch (Exception e) {
			testStepFailed("Could not remove products from cart");
			e.printStackTrace();
		}
		return productName;
	}

	/**
	 * Description: Method to verify expected message is displayed in the Alert
	 */
	public void verifyExpectedMessageAlert(String message1, String message2, String message3) {
		try {
			waitForElementToDisplay(alertMessage, 15);
			if (isElementDisplayed(alertMessage)) {
				highLighterMethod(alertMessage);
				scrollUp();
				manualScreenshot("Alert message displayed");
				String value = getText(alertMessage);
				if (value.contains(message1) && value.contains(message2) && value.contains(message3))
					testStepInfo("Relevant Alert Message was displayed");
				else
					testStepFailed("Relevant Alert Message was not displayed");

			} else
				testStepFailed("No Alert message was displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate expected Messages");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify expected message is displayed in the Alert for
	 * Invalid Coupon
	 */
	public void verifyExpectedMessageAlert_InvalidCoupon(String message1, String message2, String message3) {
		try {
			if (isElementDisplayed(alertMessage_invalidCoupon)) {
				highLighterMethod(alertMessage_invalidCoupon);
				manualScreenshot("Alert Message is displayed");
				if (getText(alertMessage_invalidCoupon).contains(message1)
						&& getText(alertMessage_invalidCoupon).contains(message2)
						&& getText(alertMessage_invalidCoupon).contains(message3))
					testStepInfo("Relevant Alert Message was displayed");
				else
					testStepFailed("Relevant Alert Message was not displayed");

			} else
				testStepFailed("No Alert message was displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate expected messages");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void updateCartQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				findWebElements(quantityUpdateBox).get(0).clear();
				findWebElements(quantityUpdateBox).get(0).sendKeys(String.valueOf(quantity));
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("Could not update quantity of the product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Remove Coupon, if present
	 */
	public void removeCoupon() {
		try {
			if (findWebElements(removeCoupon).size() > 0) {
				highLighterMethod(removeCoupon);
				clickOn(removeCoupon);
				waitForElement(alertMessage);
			} else
				testStepInfo("Remove Coupon was not present");
		} catch (Exception e) {
			testStepFailed("Remove Coupon could not be performed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the cart quantity with an expected quantity.
	 */
	public void checkCartQuantity(int number) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				int quantity = Integer.parseInt(getAttributeValue(quantityUpdateBox, "value"));
				if (quantity == number) {
					testStepInfo("The quantity expected is present");
				}
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("The quantity expected is not present");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the status of Update Cart - disabled or enabled
	 */
	public void checkStatusUpdateCart() {
		try {
			if (getAttributeValue(updateCart, "aria-disabled").contains("true")) {
				testStepInfo("The Update Cart button is disabled");
			} else
				testStepInfo("The Update Cart button is not disabled");
		} catch (Exception e) {
			testStepInfo("The Cart button status could not fetched");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Update Cart
	 */
	public void clickUpdateCart() {
		try {
			checkStatusUpdateCart();
			if (isElementDisplayed(updateCart)) {
				highLighterMethod(updateCart);
				clickOn(updateCart);
			} else
				testStepFailed("The Update Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Update Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter the coupon code
	 * 
	 */
	public void enterCouponCode(String couponCode_data) {
		try {
			if (isElementDisplayed(couponCode)) {
				highLighterMethod(couponCode);
				typeIn(couponCode, couponCode_data);
			} else
				testStepFailed("The Coupon Code text box was not displayed");
		} catch (Exception e) {
			testStepFailed("The Coupon Code could not be entered");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Apply Coupon
	 */
	public void clickApplyCoupon() {
		try {
			if (isElementDisplayed(applyCoupon)) {
				highLighterMethod(applyCoupon);
				clickOn(applyCoupon);
			} else
				testStepFailed("The Apply Coupon button was not displayed");
		} catch (Exception e) {
			testStepFailed("Apply Coupon could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to remove all products in My Cart page
	 */
	public void removeAllProducts() {
		List<WebElement> removeProducts;
		try {
			removeProducts = new ArrayList<WebElement>();
			removeProducts = findWebElements(removeProduct);
			int totalProducts = removeProducts.size();

			if (totalProducts > 0) {
				GOR.productAdded = false;
				for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
					removeProducts = new ArrayList<WebElement>();
					removeProducts = findWebElements(removeProduct);
					for (WebElement removeTheProduct : removeProducts) {
						highLighterMethod(removeTheProduct);
						waitForElementToDisplay(removeProduct, 20);
						removeTheProduct.click();
						waitForElementToDisplay(alertMessage, 20);
						break;
					}
					if (removeCounter != totalProducts - 1) {
						removeProducts = new ArrayList<WebElement>();
						removeProducts = findWebElements(MyCart.removeProduct);
					}
				}
				testStepInfo("All Products Removed");
			} else
				testStepInfo("No Products in Cart");
		} catch (Exception e) {
			testStepFailed("All products removal from cart was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Proceed to Checkout
	 */
	public void clickProceedToCheckOut() {
		try {
			if (isElementDisplayed(proceedCheckout)) {
				highLighterMethod(proceedCheckout);
				clickOn(proceedCheckout);
			} else
				testStepFailed("Proceed to Checkout was not displayed");
		} catch (Exception e) {
			testStepFailed("Proceed to checkout could not be clicked");
			e.printStackTrace();
		}
	}

}
