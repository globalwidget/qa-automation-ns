package pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Cart_AllProducts extends ApplicationKeywords {

	private static final String getAllProducts_NonCapsuleProduct = "Non Capsules Products #xpath=//ul[contains(@class,'products')]//li[not(contains(@class,'capsules'))]";
	private static final String getAllProducts_addToCartInPGP = "Products with Add To Cart in PGP #xpath=//a[contains(text(),'Add to cart')]";
	private static final String addToCart = "Add To Cart #xpath=//button[contains(text(),'Add to cart')]";
	private static final String selectGummyCount = "Select Gummy Count #xpath=//select[@id='pa_gummy-count']";
	private static final String viewCart = "View Cart #xpath=//a[contains(text(),'View cart')]";
	private static final String title_selectedProduct = "Title of product selected #xpath=//h1[contains(@class,'title')]";
	private static final String productAddedCart_PGP = "Title of product in cart from PGP #xpath=//a[contains(text(),'View cart')]/../a/h2";

	public Cart_AllProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to select the first product on the page
	 */
	public void selectFirstProduct() {
		try {
			if (isElementDisplayed(getAllProducts_NonCapsuleProduct)) {
				java.util.List<WebElement> allCapsules = new ArrayList<WebElement>();
				allCapsules = findWebElements(getAllProducts_NonCapsuleProduct);
				allCapsules.get(0).click();
				testStepInfo("The first product under Capsules is clicked");
			} else {
				testStepFailed("Products were not displayed under Capsules");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to add the first product on the PGP with AddToCart
	 */
	public String selectFirstProductWithAddToCart() {
		try {
			if (isElementDisplayed(getAllProducts_addToCartInPGP)) {
				java.util.List<WebElement> allCapsules = new ArrayList<WebElement>();
				allCapsules = findWebElements(getAllProducts_addToCartInPGP);
				allCapsules.get(0).click();
				testStepInfo("The first product with Add To Cart is clicked");
				waitForElementToDisplay(viewCart, 5);
				if (isElementDisplayed(viewCart)) {
					testStepInfo("The first product with Add To Cart in PGP is added to cart successfully");
					scrollToViewElement(productAddedCart_PGP);
					String title = getText(productAddedCart_PGP);
					return title;
				}

			} else {
				testStepFailed("Products with Add To Cart were not displayed in PGP");
			}
		} catch (Exception e) {
			testStepFailed("A Product with Add To Cart in PGP could not be selected");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to select the second product on the page
	 */
	public void selectSecondProduct() {
		try {
			if (isElementDisplayed(getAllProducts_NonCapsuleProduct)) {
				java.util.List<WebElement> allCapsules = new ArrayList<WebElement>();
				allCapsules = findWebElements(getAllProducts_NonCapsuleProduct);
				allCapsules.get(2).click();
				testStepInfo("The first product under Capsules is clicked");
			} else {
				testStepFailed("Products were not displayed under Capsules");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to choose Gummy Count
	 */
	public void chooseFirstGummyCount() {
		try {
			if (findWebElements(selectGummyCount).size() > 0) {
				if (isElementDisplayed(selectGummyCount)) {
					highLighterMethod(selectGummyCount);
					selectFromDropdown(selectGummyCount, 1);
					testStepInfo("First option under Gummy Count was selected");
				} else {
					testStepFailed("Choose Gummy Count  - was not displayed");
				}
			} else {
				testStepInfo("Choose Gummy Count  - was not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not choose the Gummy Count");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart
	 */
	public void clickAddToCart() {
		try {
			if (isElementDisplayed(addToCart)) {
				highLighterMethod(addToCart);
				clickOn(addToCart);
				testStepInfo("The add to cart button was clicked");
			} else {
				testStepFailed("The add to cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on View Cart
	 */
	public void clickViewCart() {
		try {
			if (isElementDisplayed(viewCart)) {
				highLighterMethod(viewCart);
				clickOn(viewCart);
				GOR.productAdded = true;
				testStepInfo("The View Cart button was clicked");
			} else {
				testStepFailed("The View Cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("View cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the product after selecting it.
	 */
	public String getProductTitle() {
		try {
			if (isElementDisplayed(title_selectedProduct)) {
				highLighterMethod(title_selectedProduct);
				return getText(title_selectedProduct);
			} else {
				testStepFailed("Product Title of the selected product not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product title");
			e.printStackTrace();
		}
		return "";
	}
}
