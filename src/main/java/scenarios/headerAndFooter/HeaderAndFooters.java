package scenarios.headerAndFooter;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class HeaderAndFooters extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	pages.HeaderAndFooters headerAndFooters;
	Cart_AllProducts cart_AllProducts;
	MyCart mycart;
	private boolean status = false;

	String password;

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		headerAndFooters = new pages.HeaderAndFooters(obj);
		mycart = new MyCart(obj);
	}

	/*
	 * TestCaseid : General Description : Verify all the header menus are navigating
	 * to the respective pages
	 */
	public void verifyHeader() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headerAndFooters.Verifypresence_PhoneNumber();
			headerAndFooters.clickFacebookIcon_Header();
			headerAndFooters.verifyNavigation("facebook");
			headerAndFooters.clickInstagramIcon_Header();
			headerAndFooters.verifyNavigation("instagram");
			headerAndFooters.clickTwitterIcon_Header();
			headerAndFooters.verifyNavigation("twitter");
			headerAndFooters.clickYoutubeIcon_Header();
			headerAndFooters.verifyNavigation("youtube");
			if (GOR.loggedIn == true) {
				headerAndFooters.goToMyAccount();
				myaccount.clickOnLogout();
			}
			headerAndFooters.goToMyAccount();
			login_register.login(retrieve("loginUsername"), retrieve("loginPassword"));
			headerAndFooters.goToMyAccount();
			myaccount.clickOnLogout();
			headerAndFooters.clickOnMyCart();
			headerAndFooters.verifyNavigation("cart");
			mycart.removeAllProducts();
			headerAndFooters.verifyNavigation("emptyCart");
			headerAndFooters.goTo_Home();
			headerAndFooters.verifyNavigation("homeBanner");
			headerAndFooters.goTo_Shop(0, 0);
			headerAndFooters.verifyNavigation("shop");
			headerAndFooters.goTo_Shop(1, 0);
			headerAndFooters.verifyNavigation("newCBDProducts");
//			headerAndFooters.goTo_Shop(2, 0);
//			headerAndFooters.verifyNavigation("cbdEdibles");
//			headerAndFooters.goTo_Shop(2, 1);
//			headerAndFooters.verifyNavigation("Gummies");
//			headerAndFooters.goTo_Shop(2, 2);
//			headerAndFooters.verifyNavigation("capsules");
//			headerAndFooters.goTo_Shop(2, 3);
//			headerAndFooters.verifyNavigation("Shop_cbdOil");
//			headerAndFooters.goTo_Shop(2, 4);
//			headerAndFooters.verifyNavigation("cbdSyrup");
//			headerAndFooters.goTo_Shop(3, 0);
//			headerAndFooters.verifyNavigation("cbdTopicals");
//			headerAndFooters.goTo_Shop(3, 1);
//			headerAndFooters.verifyNavigation("cbdPainReliefGel");
//			headerAndFooters.goTo_Shop(3, 2);
//			headerAndFooters.verifyNavigation("cbdLipBalm");
//			headerAndFooters.goTo_Shop(3, 3);
//			headerAndFooters.verifyNavigation("cbdBathBombs");
//			headerAndFooters.goTo_Shop(3, 4);
//			headerAndFooters.verifyNavigation("cbdEssentialOilRollers");
//			headerAndFooters.goTo_Shop(3, 5);
//			headerAndFooters.verifyNavigation("cbdHeatReliefSpray");
//			headerAndFooters.goTo_Shop(3, 6);
//			headerAndFooters.verifyNavigation("cbdLotion");
//			headerAndFooters.goTo_Shop(3, 7);
//			headerAndFooters.verifyNavigation("cbdPatches");
//			headerAndFooters.goTo_Shop(4, 0);
//			headerAndFooters.verifyNavigation("petCBDproducts");
//			headerAndFooters.goTo_Shop(4, 1);
//			headerAndFooters.verifyNavigation("petCBDdogtreats");
//			headerAndFooters.goTo_Shop(4, 2);
//			headerAndFooters.verifyNavigation("petCBDoil");
			headerAndFooters.goTo_Shop(5, 0);
			headerAndFooters.verifyNavigation("Shop_Gummies");
			headerAndFooters.goTo_Shop(6, 0);
			headerAndFooters.verifyNavigation("Shop_cbdOil");
			headerAndFooters.goTo_Shop(7, 0);
			headerAndFooters.verifyNavigation("Shop_capsules");
			headerAndFooters.goTo_Shop(8, 0);
			headerAndFooters.verifyNavigation("Shop_cbdPainReliefRub");

			headerAndFooters.goTo_Wholesale(0);
			headerAndFooters.verifyNavigation("wholesale");
			headerAndFooters.goTo_Wholesale(1);
			headerAndFooters.verifyNavigation("wholesaleTermsConditions");
			headerAndFooters.goTo_Wholesale(2);
			headerAndFooters.verifyNavigation("wholesale");

			headerAndFooters.goTo_Learn(0);
			headerAndFooters.verifyNavigation("learn");
//			headerAndFooters.goTo_Learn(1);
//			headerAndFooters.verifyNavigation("productFinderQuiz");
			headerAndFooters.goTo_Learn(2);
			headerAndFooters.verifyNavigation("ourStory");
			headerAndFooters.goTo_Learn(3);
			headerAndFooters.verifyNavigation("cbdblog");
			headerAndFooters.goTo_Learn(4);
			headerAndFooters.verifyNavigation("CBDoilbenefits");
			headerAndFooters.goTo_Learn(5);
			headerAndFooters.verifyNavigation("intheNews");
			headerAndFooters.goTo_Learn(6);
			headerAndFooters.verifyNavigation("hempRecipe");
			headerAndFooters.goTo_Learn(7);
			headerAndFooters.verifyNavigation("labTesting");
			headerAndFooters.goTo_Learn(8);
			headerAndFooters.verifyNavigation("faq");

			headerAndFooters.goTo_Contact();
			headerAndFooters.verifyNavigation("contact");
			headerAndFooters.Verify_PrivacyPolicy();
			headerAndFooters.goTo_Newsletter();
			headerAndFooters.verifyNavigation("newsletter");

		} catch (Exception e) {
			testStepFailed("Header Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndFooters.testFailure
				|| mycart.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General Description : Verify all the footer menus are navigating
	 * to the respective pages
	 */
	public void verifyFooter() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			headerAndFooters.navigateFooter("home");
			headerAndFooters.verifyNavigation("homeBanner");
			headerAndFooters.navigateFooter("shop");
			headerAndFooters.verifyNavigation("shop");
			headerAndFooters.navigateFooter("wholesale");
			headerAndFooters.verifyNavigation("wholesale");
			headerAndFooters.navigateFooter("newsletter");
			headerAndFooters.verifyNavigation("newsletter");
			headerAndFooters.navigateFooter("shippingPolicy");
			headerAndFooters.verifyNavigation("shippingPolicy");
			headerAndFooters.navigateFooter("terms");
			headerAndFooters.verifyNavigation("terms");
			headerAndFooters.navigateFooter("faq");
			headerAndFooters.verifyNavigation("faq");
			headerAndFooters.navigateFooter("refund");
			headerAndFooters.verifyNavigation("refundPolicy");
			headerAndFooters.navigateFooter("contact");
			headerAndFooters.verifyNavigation("contact");
			headerAndFooters.navigateFooter("ccpa");
			headerAndFooters.verifyNavigation("ccpa");

			headerAndFooters.navigateFooter("facebook");
			headerAndFooters.verifyNavigation("facebook");
			headerAndFooters.navigateFooter("instagram");
			headerAndFooters.verifyNavigation("instagram");
			headerAndFooters.navigateFooter("twitter");
			headerAndFooters.verifyNavigation("twitter");
			headerAndFooters.navigateFooter("youtube");
			headerAndFooters.verifyNavigation("youtube");
			headerAndFooters.navigateFooter("pinterest");
			headerAndFooters.verifyNavigation("pinterest");
			headerAndFooters.navigateFooter("premiumHempbombs");
			headerAndFooters.verifyNavigation("homeBanner");

		} catch (Exception e) {
			testStepFailed("Footer Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
