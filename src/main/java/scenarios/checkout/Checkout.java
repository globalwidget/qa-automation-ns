package scenarios.checkout;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class Checkout extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	pages.Checkout checkout;
	HeaderAndFooters headerAndfooter;

	private boolean status = false;
	HashMap<String, String> billingData;
	HashMap<String, String> shippingData;
	String Username;
	String Password;
	String cardNumber_data;
	String expirationMonth_data;
	String expirationYear_data;
	String cardSecurityCode_data;
	String detailsSaved;
	String shipDifferentAddress;
	String password;

	public Checkout(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
		checkout = new pages.Checkout(obj);
		headerAndfooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is not able to
	 * place the order with any invalid details in the checkout page.
	 */
	public void checkout_InvalidDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");
			detailsSaved = retrieve("detailsSaved");
			String invalidEmailAddress = retrieve("invalidEmailAddress");
			String invalidBillingPostCode = retrieve("invalidBillingPostCode");
			String invalidShippingPostCode = retrieve("invalidShippingPostCode");
			String invalidphonenumber = retrieve("invalidphonenumber");

			billingData = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();
			GOR.shipDifferentAddress = true;

			if (detailsSaved.equalsIgnoreCase("no")) {
				billingData.put("firstName_data", retrieve("firstName_Billing"));
				billingData.put("lastName_data", retrieve("lastName_Billing"));
				billingData.put("companyName_data", retrieve("companyName_Billing"));
				billingData.put("billingCountry_data", retrieve("country_Billing"));
				billingData.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingData.put("city_data", retrieve("city_Billing"));
				billingData.put("state_data", retrieve("state_Billing"));

				shippingData.put("firstName_data", retrieve("firstName_shipping"));
				shippingData.put("lastName_data", retrieve("lastName_shipping"));
				shippingData.put("companyName_data", retrieve("companyName_shipping"));
				shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
				shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
				shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
				shippingData.put("city_data", retrieve("city_shipping"));
				shippingData.put("state_data", retrieve("state_shipping"));

			}
			billingData.put("postCode_data", invalidBillingPostCode);
			shippingData.put("postCode_data", invalidShippingPostCode);
			billingData.put("email_data", invalidEmailAddress);
			billingData.put("phoneNumber_data", invalidphonenumber);

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loginFreeRun == false) {
				if (GOR.loggedIn == false) {
					testStepInfo("User is not logged In");
				} else {
					headerAndfooter.goToMyAccount();
					myaccount.clickOnLogout();
				}
			}

			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseFirstGummyCount();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.ClearDetails();
			checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
					cardSecurityCode_data);
			checkout.clickPlaceOrder();
			checkout.VerifyEmptyFieldErrors();
//			homePage.closeOfferPopup();
			if (GOR.loginFreeRun == false) {
				checkout.Login(Username, Password);
			}
			checkout.fillDetails(billingData, shippingData);
			checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationMonth_data,
					cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			scrollUp();
			checkout.verifyErrorRelevantMessage("Invalid Email");
			billingData.replace("email_data", retrieve("email"));
			shippingData.replace("postCode_data", null);
			billingData.replace("postCode_data", null);
			billingData.replace("phoneNumber_data", null);
			checkout.fillDetails(billingData, shippingData);
			checkout.clickPlaceOrder();
			scrollUp();
			checkout.verifyErrorRelevantMessage("Invalid Billing Post Code");
			checkout.verifyErrorRelevantMessage("Invalid Shipping Post Code");
			checkout.verifyErrorRelevantMessage("Invalid phonenumber");

		} catch (Exception e) {
			testStepFailed("Placing Order with Invalid Details could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to place
	 * the order with any invalid card details in the checkout page.
	 */
	public void checkout_InvalidCreditCardDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");
			detailsSaved = retrieve("detailsSaved");
			String cardNumber_Wronglength = retrieve("cardNumberWrongLength");
			String expirationYear_invalidData = retrieve("expirationYearInvalidData");
			String cardSecurityCode_invalidData = retrieve("cardSecurityCodeInvalidData");

			billingData = new HashMap<String, String>();
			GOR.shipDifferentAddress = false;
			if (detailsSaved.equalsIgnoreCase("yes") == false) {
				billingData.put("firstName_data", retrieve("firstName_Billing"));
				billingData.put("lastName_data", retrieve("lastName_Billing"));
				billingData.put("companyName_data", retrieve("companyName_Billing"));
				billingData.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingData.put("billingCountry_data", retrieve("country_Billing"));
				billingData.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingData.put("postCode_data", retrieve("postCode_Billing"));
				billingData.put("city_data", retrieve("city_Billing"));
				billingData.put("state_data", retrieve("state_Billing"));
				billingData.put("email_data", retrieve("emailAddress"));

			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}
			if (GOR.loginFreeRun == false) {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(Username, Password);
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFirstGummyCount();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			GOR.shipDifferentAddress = false;
			checkout.clickShipDifferentAddress(false);
			checkout.ClearDetails();
			checkout.clickPlaceOrder();
			checkout.VerifyEmptyCardDetailsErrors();
			checkout.fillDetails(billingData, shippingData);
			checkout.fillDetailsforCreditCard(cardNumber_Wronglength, expirationMonth_data, expirationYear_invalidData,
					cardSecurityCode_invalidData);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			scrollUp();
			checkout.verifyErrorRelevantMessage("Wrong Length Card Number");
			checkout.verifyErrorRelevantMessage("Invalid Card Number");
			checkout.verifyErrorRelevantMessage("Invalid Security Code");
			checkout.verifyErrorRelevantMessage("Invalid Expiration Data");

		} catch (Exception e) {
			testStepFailed("Placing Order with invalid card details could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to place
	 * the order with valid details in the checkout page.
	 */
	public void checkout_ValidDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			billingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = false;

			billingData.put("firstName_data", retrieve("firstName_Billing"));
			billingData.put("lastName_data", retrieve("lastName_Billing"));
			billingData.put("companyName_data", retrieve("companyName_Billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingData.put("billingCountry_data", retrieve("country_Billing"));
			billingData.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingData.put("postCode_data", retrieve("postCode_Billing"));
			billingData.put("city_data", retrieve("city_Billing"));
			billingData.put("state_data", retrieve("state_Billing"));
			billingData.put("email_data", retrieve("emailAddress"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFirstGummyCount();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			checkout.clickShipDifferentAddress(false);
			checkout.fillDetails(billingData, shippingData);
			checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
					cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();

		} catch (Exception e) {
			testStepFailed("Placing Order with Valid Details could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the details saved in the
	 * account is displayed in the checkout page
	 */
	public void checkout_VerifySavedDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			HashMap<String, String> billingData_Saved = new HashMap<String, String>();
			HashMap<String, String> shippingData_Saved = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.goToMyAccount();
			myaccount.clickOnAddresses();
			billingData_Saved = myaccount.getSavedBilllingDetails();
			myaccount.clickOnAddresses();
			shippingData_Saved = myaccount.getSavedShippingDetails();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFirstGummyCount();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			checkout.clickShipDifferentAddress(true);
			checkout.checkSavedDetails(billingData_Saved, shippingData_Saved);
		} catch (Exception e) {
			testStepFailed(
					"Details saved in the account for checkout are reflected in the checkout page could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if a guest user can enter
	 * details for checkout
	 */
	public void checkout_GuestUser() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			billingData = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingData.put("firstName_data", retrieve("firstName_Billing"));
			billingData.put("lastName_data", retrieve("lastName_Billing"));
			billingData.put("companyName_data", retrieve("companyName_Billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingData.put("billingCountry_data", retrieve("country_Billing"));
			billingData.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingData.put("postCode_data", retrieve("postCode_Billing"));
			billingData.put("city_data", retrieve("city_Billing"));
			billingData.put("state_data", retrieve("state_Billing"));
			billingData.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes_data", retrieve("orderNotes"));

			GOR.shipDifferentAddress = true;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loginFreeRun == false) {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(Username, Password);
					myaccount.clickOnLogout();
				} else {
					headerAndfooter.goToMyAccount();
					myaccount.clickOnLogout();
				}
			}

			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseFirstGummyCount();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingData, shippingData);

		} catch (Exception e) {
			testStepFailed("Guest user can enter details for checkout could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to ship to
	 * a different address other than the billing address on the checkout flow
	 */
	public void checkout_ShipDifferentAddress() {
		try {
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			billingData = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingData.put("firstName_data", retrieve("firstName_Billing"));
			billingData.put("lastName_data", retrieve("lastName_Billing"));
			billingData.put("companyName_data", retrieve("companyName_Billing"));
			billingData.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingData.put("billingCountry_data", retrieve("country_Billing"));
			billingData.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingData.put("postCode_data", retrieve("postCode_Billing"));
			billingData.put("city_data", retrieve("city_Billing"));
			billingData.put("state_data", retrieve("state_Billing"));
			billingData.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes_data", retrieve("orderNotes"));

			GOR.shipDifferentAddress = true;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFirstGummyCount();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingData, shippingData);
			checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
					cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();
		} catch (Exception e) {
			testStepFailed(
					"User is able to ship to different address other than billing address could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
