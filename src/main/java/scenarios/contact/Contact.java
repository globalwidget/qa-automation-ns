package scenarios.contact;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.ContactUs;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Contact extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	HeaderAndFooters headerAndFooters;
	ContactUs contactUs;
	private boolean status = false;

	String password;

	public Contact(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		contactUs = new ContactUs(obj);
	}

	/*
	 * TestCaseid : Contact Description : Verify proper error message is displayed
	 * for the invalid fields in the form
	 */
	public void submitInvalidDetails() {
		try {
			
			password = retrieve("securityPassword");
			int department_Index = 8;
			String InvalidEmail = retrieve("invalidEmail");
			String Invalidphone = retrieve("invalidPhone");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_Contact();
			contactUs.fillDetails(0, " ", " ", " ", " ", " ", " ");
			contactUs.clickSend();
			contactUs.checkEmptyFieldErrors();
			contactUs.fillDetails(department_Index, " ", " ", " ", InvalidEmail, Invalidphone, " ");
			contactUs.clickSend();
			contactUs.checkInvalidDataErrors();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Contact Description :Verify if the user is able to submit the
	 * contact form
	 */
	public void submitValidDetails() {
		try {
			password = retrieve("securityPassword");
			int department_Index = 8;
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String businessName = retrieve("businessName");
			String email = retrieve("email");
			String phone = retrieve("phone");
			String message = retrieve("message");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false)
				homePage.closeOfferPopup();
			headerAndFooters.goTo_Contact();
			contactUs.fillDetails(department_Index, firstName, lastName, businessName, email, phone, message);
			contactUs.clickSend();
			contactUs.verifySuccessMessage();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
