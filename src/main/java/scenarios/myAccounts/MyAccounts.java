package scenarios.myAccounts;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class MyAccounts extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	pages.Checkout checkout;
	HeaderAndFooters headerAndfooter;

	private boolean status = false;
	HashMap<String, String> billingdata;
	HashMap<String, String> shippingData;
	String Username;
	String Password;
	String cardNumber_data;
	String expirationMonth_data;
	String expirationYear_data;
	String cardSecurityCode_data;
	String detailsSaved;
	String shipDifferentAddress;
	String password;

	public MyAccounts(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
		checkout = new pages.Checkout(obj);
		headerAndfooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_067_076 Description : To Verify if the
	 * user is able to navigate to account dashboard page and verify if the user is
	 * able to view the account details.
	 */

	public void myAccount_viewAccountDetails() {
		try {
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			headerAndfooter.verifyNavigation("myAccountHeader");
			myaccount.clickOnAccountDetails();
			myaccount.checkAccountDetailsDisplay();

		} catch (Exception e) {
			testStepFailed("Navigation to My Account page and Account Details display could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure
				|| login_register.testFailure || login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_079 Description : To Verify proper
	 * validation error message is displayed upon saving account details with any
	 * invalid data.
	 */

	public void myAccount_invalidDataError() {
		try {
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			String currentPassword = retrieve("currentPassword");
			String newPassword = retrieve("newPassword");
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			headerAndfooter.verifyNavigation("myAccountHeader");
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainAccountDetailsFields(" ", " ", " ", " ");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("emailAddressMissing");
			myaccount.enterDatainAccountDetailsFields("", "", "", "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkEmptyFieldErrorMessages();
			myaccount.enterDatainPasswordFields(currentPassword, "", "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("fillAllPasswordfieldsError");
			myaccount.enterDatainPasswordFields(currentPassword, newPassword, "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("reEnterPasswordError");
			myaccount.enterDatainPasswordFields("", newPassword, "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("enterCurrentPasswordError");
			myaccount.enterDatainPasswordFields(" ", newPassword, " ");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("passwordsDoNotMatch");

		} catch (Exception e) {
			testStepFailed("The error messages for invalid account details could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure
				|| login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_077 Description : To Verify if the user
	 * is able to save the basic details in the account details section.
	 */
	public void myAccount_saveBasicDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			String displayName = retrieve("displayName");
			String email = retrieve("email");
			String oldFirstName = retrieve("oldFirstName");
			String oldLastName = retrieve("oldLastName");
			String oldDisplayName = retrieve("oldDisplayName");
			String oldEmail = retrieve("oldEmail");
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			headerAndfooter.verifyNavigation("myAccountHeader");
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainAccountDetailsFields(firstName, lastName, displayName, email);
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainAccountDetailsFields(oldFirstName, oldLastName, oldDisplayName, oldEmail);
			myaccount.clickOnSaveChangesMyAccounts();
		} catch (Exception e) {
			testStepFailed("Editing basic details of a account could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure
				|| login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_078 Description : To Verify is the user
	 * is able to change the password.
	 */
	public void myAccount_ChangePassword() {
		try {
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			String oldPassword = retrieve("oldPassword");
			String newPassword = retrieve("newPassword");
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainPasswordFields(oldPassword, newPassword, newPassword);
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainPasswordFields(newPassword, oldPassword, oldPassword);
			myaccount.clickOnSaveChangesMyAccounts();

		} catch (Exception e) {
			testStepFailed("Changing password of the account could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
